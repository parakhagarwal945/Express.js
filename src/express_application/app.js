const path=require("path")
const express=require("express")
const app=express()
app.get('/',(req,res)=>{
    var a=path.join(__dirname,"index.html")
    res.sendFile(a)
})
app.get('/login',(req,res)=>{
    var b=path.join(__dirname,"login.html")
    res.sendFile(b)
})
app.get('/signup',(req,res)=>{
    var c=path.join(__dirname,"signUp.html")
    res.sendFile(c)
})
app.get('/aboutus',(req,res)=>{
    var d=path.join(__dirname,"aboutUs.html")
    res.sendFile(d)
})
app.listen(3017)
module.exports=app