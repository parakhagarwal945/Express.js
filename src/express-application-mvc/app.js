const express = require("express");

// importing routes that will be serving client
const routes = require("./backend/routes/htmlRoutes");

// defining object of express as app.
const app = express();

app.use("/", routes);

app.listen(4053)

module.exports = app;
