const path = require("path");

let homepage = (req, res) => {
  let a = path.join(__dirname, "../../client/views/homepage.html");
  res.sendFile(a);
};


let aboutus = (req, res) => {
  let b = path.join(__dirname, "../../client/views/aboutus.html");
  res.sendFile(b);
};

let login = (req, res) => {
  let c = path.join(__dirname, "../../client/views/login.html");
  res.sendFile(c);
};


let signup = (req, res) => {
  let d = path.join(__dirname, "../../client/views/signup.html");
  res.sendFile(d);
};

module.exports = {
  homepage: homepage,
  aboutus: aboutus,
  login: login,
  signup: signup
};
