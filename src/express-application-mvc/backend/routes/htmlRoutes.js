// importing express module
const express = require("express");

// importing our controller file so as to map them to their routes
const pages = require("../controller/htmlController");

const router = express.Router();

router.route("/").get(pages.homepage);

router.route("/aboutus").get(pages.aboutus);

router.route("/login").get(pages.login);

router.route("/signup").get(pages.signup);

module.exports = router;
